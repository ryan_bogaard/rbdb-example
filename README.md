# RBDB-example

## Config
* `projectId` : copy project id from https://gitlab.com/__USERNAME_OR_GROUP__/__PROJECT_NAME__/edit (under `Settings`>`General` on gitlab)
* `mainBranch` : alternative branch name usually master

```json
{
    "projectId": "37392544", 
    "allowCMS": true,
    "isCompressed": false,
    "mainBranch": "main" 
}
```

## Database example
```json
{
    "index": 1,
    "structure": [
        { "name": "first_name", "nullable": false },
        { "name": "last_name", "nullable": false },
        { "name": "home_addresses", "type":"addresses[]", "nullable": true }
    ],
    "data":[
        {
            "_id": 0,
            "first_name": "John",
            "last_name": "Doe",
            "home_addresses": [0, 1]
        }
    ]
}
```
